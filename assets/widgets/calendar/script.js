$(function() {

	/*DATE TIME OBJECT*/
	var dt = new DT();

	dt.build_obj(
		parseInt($( "#year" ).val(),10),
		parseInt($( "#month" ).val(),10),
		parseInt($( "#day" ).val(),10),
		parseInt($( "#hour" ).val(),10),
		parseInt($( "#minute" ).val(),10),
		parseInt($( "#second" ).val(),10),
		$( "#date" ).val(),
		$( "#time" ).val(),
		$( "#date_time" ).val()
	);

	/*DATEPICKER DATE*/
	var dp = $( ".datepicker" );
	dp.datepicker( { 

		dateFormat: "yy-mm-dd",
		defaultDate: $('#date').val(),
		
		onSelect: function(event, ui){

			var year = parseInt(ui.selectedYear,10);
			var month = parseInt(ui.selectedMonth,10) + 1;/*mesecite pocnuvaat od 0 do 11*/
			var day = parseInt(ui.selectedDay,10);
			$( "#year" ).val(year);
			$( "#month" ).val(month);
			$( "#day" ).val(day);

			var sMonth = (month < 10)? '0'+month : month;
			var sDay = (day < 10)? '0'+day : day;
			$( "#date" ).val(year + '-' + sMonth + '-' + sDay);
			update_date_time();

			/*update dt object*/
			dt.set_year(year);
			dt.set_month(month);
			dt.set_day(day);
			dt.set_date($( "#date" ).val());
			dt.set_date_time($( "#date_time" ).val());

		}
		
	} );/*end datepicker*/


	/* SPINNER TIME*/
	var hour = parseInt($( "#hour" ).val(),10);
	var sHour	= (hour < 10)? '0'+hour : hour;
	var minute = parseInt($( "#minute" ).val(),10);
	var sMinute	= (minute < 10)? '0'+minute : minute;
	var second = parseInt($( "#second" ).val(),10);
	var sSecond	= (second < 10)? '0'+second : second;
	
	$( "#hour" ).spinner({
		min: 0,
		max: 23,
		spin: function( event, ui ) {
			var hour = parseInt(ui.value,10);
			sHour = (hour < 10)? '0'+hour : hour;
			$( "#time" ).val(sHour + ':' + sMinute + ':' + sSecond);
			update_date_time();

			dt.set_hour(hour);
			dt.set_time($( "#time" ).val());
			dt.set_date_time($( "#date_time" ).val());
		}
	});
	$( "#minute" ).spinner({
		min: 0,
		max: 59,
		spin: function( event, ui ) {
			var minute = parseInt(ui.value,10);
			sMinute = (minute < 10)? '0'+minute : minute;
			$( "#time" ).val(sHour + ':' + sMinute + ':' + sSecond);
			update_date_time();

			dt.set_minute(minute);
			dt.set_time($( "#time" ).val());
			dt.set_date_time($( "#date_time" ).val());
		}
	});
	$( "#second" ).spinner({
		min: 0,
		max: 59,
		spin: function( event, ui ) {
			var second = parseInt(ui.value,10);
			sSecond = (second < 10)? '0'+second : second;
			$( "#time" ).val(sHour + ':' + sMinute + ':' + sSecond);
			update_date_time();

			dt.set_second(second);
			dt.set_time($( "#time" ).val());
			dt.set_date_time($( "#date_time" ).val());
		}
	});


	$('#modify_dt').on('click', function(){

		var myJSON = JSON.stringify({dt: dt.get_obj()});
		var url = $( "body" ).attr('data-site-url') + 'calendar/form_ajax';

		$.ajax({
	        url: url,
	        type: 'POST',
	        dataType: "json",//OVA E BITNO!!!
	        data: { json_data: myJSON }
	    }).done(function( data ) {
		    
		    $( ".datepicker" ).datepicker( "setDate", data.date );
		    $( ".datepicker" ).datepicker( "refresh" );

			dt.build_obj(
				parseInt(data.year,10),
				parseInt(data.month,10),
				parseInt(data.day,10),
				parseInt(data.hour,10),
				parseInt(data.minute,10),
				parseInt(data.second,10),
				data.pdate,
				data.time,
				data.date_time
			);

			/*Ubdate form data with new object*/
			update_form_data(dt);

		});/*end done ajax*/

	});/*end click*/

});/*end doc ready*/

/*Helper functions*/
function update_date_time(){
	$( "#date_time" ).val($( "#date" ).val() + ' ' + $( "#time" ).val());
}
function update_form_data(dt){
	$( "#year" ).val(dt.get_year());
	$( "#month" ).val(dt.get_month());
	$( "#day" ).val(dt.get_day());
	$( "#hour" ).val(dt.get_hour());
	$( "#minute" ).val(dt.get_minute());
	$( "#second" ).val(dt.get_second());
	$( "#date" ).val(dt.get_date());
	$( "#time" ).val(dt.get_time());
	$( "#date_time" ).val(dt.get_date_time());
}