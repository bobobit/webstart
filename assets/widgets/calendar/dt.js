var DT = function(){
	var year;
    var month;
    var day;
    var hour;
    var minute;
    var second;
    var date;
    var time;
    var date_time;

    var obj;

    var update_object = function(){
    	obj = {
				'year' 		: year,
				'month' 	: month,
				'day' 		: day,
				'hour' 		: hour,
				'minute' 	: minute,
				'second' 	: second,
				'date' 		: date,
				'time' 		: time,
				'date_time' : date_time
			};
    };

	return {
		set_year: function(iyear){
			year = iyear;
			update_object();
		},
		get_year: function(){
			return year;
		},
		set_month: function(imonth){
			month = imonth;
			update_object();
		},
		get_month: function(){
			return month;
		},
		set_day: function(iday){
			day = iday;
			update_object();
		},
		get_day: function(){
			return day;
		},
		set_hour: function(ihour){
			hour = ihour;
			update_object();
		},
		get_hour: function(){
			return hour;
		},
		set_minute: function(iminute){
			minute = iminute;
			update_object();
		},
		get_minute: function(){
			return minute;
		},
		set_second: function(isecond){
			second = isecond;
			update_object();
		},
		get_second: function(){
			return second;
		},
		set_date: function(idate){
			date = idate;
			update_object();
		},
		get_date: function(){
			return date;
		},
		set_time: function(itime){
			time = itime;
			update_object();
		},
		get_time: function(){
			return time;
		},
		set_date_time: function(idate_time){
			date_time = idate_time;
			update_object();
		},
		get_date_time: function(){
			return date_time;
		},
		set_obj: function(iobj){
			obj = iobj;
		},
		get_obj: function(){
			return obj;
		},
		update_obj: function(){
			return update_object();
		},
		build_obj: function(
			iyear, imonth, iday, ihour, iminute, isecond, idate, itime, idate_time
		){
			obj = {
				'year' 		: iyear,
				'month' 	: imonth,
				'day' 		: iday,
				'hour' 		: ihour,
				'minute' 	: iminute,
				'second' 	: isecond,
				'date' 		: idate,
				'time' 		: itime,
				'date_time' : idate_time
			};

			year = iyear;
			month = imonth;
			day = iday;
			hour = ihour;
			minute = iminute;
			second = isecond;
			date = idate;
			time = itime;
			date_time = idate_time;

		}
	};//end return
};/*END DT*/