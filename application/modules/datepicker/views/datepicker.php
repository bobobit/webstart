<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Datepicker - Only Month Year</title>

		<link href="<?php echo site_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
		<link href="<?php echo site_url('assets/css/style.css'); ?>" rel="stylesheet">

		<link href="<?php echo site_url('assets/widgets/datepicker-jquery-ui/jquery-ui.css'); ?>" rel="stylesheet">
		<script src="<?php echo site_url('assets/widgets/datepicker-jquery-ui/external/jquery/jquery.js'); ?>"></script>
		<script src="<?php echo site_url('assets/widgets/datepicker-jquery-ui/jquery-ui.js'); ?>"></script>

		<script src="<?php echo site_url('assets/js/bootstrap.min.js'); ?>"></script>

		<style>

			body {
				background-color: rgba(127,127,127, 0.001);
			}

			input#datepicker {
				border-radius: 3px;
				display: block;
				width: 100%;
			}

			div.ui-datepicker {
				font-size:13.2px;
			}

			.MonthDatePicker .ui-datepicker-year {
			    /* display:none;   */ 
			}

			.HideTodayButton .ui-datepicker-buttonpane .ui-datepicker-current {
			    visibility:hidden;
			}

			.hide-calendar .ui-datepicker-calendar {
				display:none!important;
				visibility:hidden!important
			}

		</style>
		<script>
			$(function() {
				$( "#datepicker" ).datepicker( {

			        hideBCalendarPanel: true,
			        showButtonPanel: true,
			        dateFormat: 'MM yy',
			        beforeShow: function(el, dp) {
					    $(this).datepicker("hide");
			            $("#ui-datepicker-div").addClass("hide-calendar");
						// $("#ui-datepicker-div").addClass('MonthDatePicker');
						$("#ui-datepicker-div").addClass('HideTodayButton');
					},
			        onClose: function(dateText, inst) { 
			            var month = $("#ui-datepicker-div .ui-datepicker-month").text();
			            var year = $("#ui-datepicker-div .ui-datepicker-year").text();
			            var milliseconds = Date.parse(month + ' 1, ' + year);//Januar 1, 1977

			            $(this).datepicker('setDate', new Date(milliseconds));

			            /*AJAX call*/
			            blog_search();
			        }
			    });

			    $( "#datepicker" ).click(function(){
			    	var height = 
			    				$( "#datepicker" ).height() + 
			    				$( "#ui-datepicker-div" ).height() + 
			    				10 + 'px';
			    	$('#iframeDatepicker', window.parent.document).css('height', height);
			    });

			});/*end doc ready*/

			/*Costom AJAX function*/
			var blog_search = function(){

				var query = $('#datepicker').val();
				var url = "<?php echo site_url('blog/posts_by_date_ajax'); ?>";

				$('#posts_by_date').slideUp(function(){
					$.post(url, { search_query: query}, function(data){
						$('#posts_by_date').html(data);
						$('#posts_by_date').slideDown();

						var height = 
									$('#blog_search_results').height() + 
									$('.hero-unit').height() + 
									50 + 'px';
						$('#iframeDatepicker', window.parent.document).css('height', height);
					});
				});
			};/*end fun blog_search*/
		</script>
	</head>
	<body>

		<div class="hero-unit">
			<input  type="text" placeholder="   click to show datepicker "  id="datepicker">
		</div>
		<!-- Ova e rezerviran div za BLOG SEARCH sodrzinata pratena od AJAX-->
        <div id="posts_by_date">
            <!-- AJAX DATA HERE -->
        </div>

	</body>
</html>