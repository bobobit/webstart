<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grocerycrud extends MX_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}

	public function index()
	{
		$data['heading'] = 'Error!';
		$data['message'] = '<p>No direct script access allowed.</p>';
		$this->load->view('error_404', $data);
	}

	private function _error_404()
	{
		$data['heading'] = 'Error!';
		$data['message'] = '<p>No direct script access allowed.</p>';
		$this->load->view('error_404', $data);
	}

	public function _admin_pages() {

		if ($this->uri->segment(1) != 'admin') {
			$this->_error_404();
			return;
		}

        try{
            
            $crud = new grocery_CRUD();
                        
            $crud->set_theme('flexigrid');
            $crud->set_subject('page');
            $crud->set_table('pages');

            $output = $crud->render();

            $this->load->view('subview', $output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function _admin_posts() {

		if ($this->uri->segment(1) != 'admin') {
			$this->_error_404();
			return;
		}

        try{
            
            $crud = new grocery_CRUD();
                        
            $crud->set_theme('flexigrid');
            $crud->set_subject('post');
            $crud->set_table('posts');
            $crud->order_by('created','desc');
            
            $crud->set_relation_n_n(
                'categories',       //1. Ime na novata kolona
                'post_category',    //2. Ime na relacionata tabela 
                'categories',       //3. Ime na drugata tabela 
                'post_id',          //4. Primaren kluc-vrska od ovaa tabela 
                'category_id',      //5. Primaren kluc-vrska do drugata tabela 
                'name'              //6. Ime na edna kolona od drugata tabela 
                );

            $output = $crud->render();

            $this->load->view('subview', $output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function _admin_categories() {

		if ($this->uri->segment(1) != 'admin') {
			$this->_error_404();
			return;
		}

        try{
            
            $crud = new grocery_CRUD();
                        
            $crud->set_theme('flexigrid');
            $crud->set_subject('Category');
            $crud->set_table('categories');

            $output = $crud->render();

            $this->load->view('subview', $output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }


    public function _admin_post_comments() {

		if ($this->uri->segment(1) != 'admin') {
			$this->_error_404();
			return;
		}

        try{
            
            $crud = new grocery_CRUD();
                        
            $crud->set_theme('flexigrid');
            $crud->set_subject('Post Comment');
            $crud->set_table('post_comments');
            $crud->display_as('post_id','Post');
            $crud->order_by('created','desc');

            $crud->callback_column($this->_unique_field_name('post_id'),
                        array($this,'_column_short_post_title'));

            $crud->set_relation('post_id','posts','title');


            $output = $crud->render();

            $this->load->view('subview', $output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    /*Using set_relation with callback_column problem (GroceryCRUD bug solved)*/
    private function _unique_field_name($field_name) {
        //This s is because is better for a string to begin with a letter and not with a number
        return 's'.substr(md5($field_name),0,8); 
    }
    public function _column_short_post_title($value,$row) {
        if (strlen($value)>30) {
            $short_title = substr($value, 0, 15) . ' [...] ' . substr($value, -15);
        } else {
            $short_title = $value;
        }
        return '<span title="' . $value . '" >'.$short_title. '</span>';
    }


    public function _admin_photos() {

		if ($this->uri->segment(1) != 'admin') {
			$this->_error_404();
			return;
		}

        try{
            
            $crud = new grocery_CRUD();
                        
            $crud->set_theme('flexigrid');
            $crud->set_subject('photo');
            $crud->set_table('photos');
            $crud->unset_add();
            $crud->change_field_type('name','readonly');
            $crud->change_field_type('url','readonly');
            $crud->change_field_type('thumb','readonly');

            $crud->callback_before_delete(array($this,'_unlink_photo'));
                       
            $crud->where('category_id',$this->uri->segment(3));

            $output = $crud->render();

            $this->load->view('subview', $output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function _unlink_photo($primary_key=0) {

    	if ($primary_key <= 0) {
    		$this->_error_404();
			return;
    	}

		if ($this->uri->segment(1) != 'admin') {
			$this->_error_404();
			return;
		}

        $photo = $this->db->where('id',$primary_key)->get('photos')->row();

        $url='assets/img/'  . $photo->url;
        $thumb_url='assets/img/thumbnails/'  . $photo->thumb;
         
        unlink($url);
        unlink($thumb_url);

        return true;
    }

    public function _admin_upload_photo() {

		if ($this->uri->segment(1) != 'admin') {
			$this->_error_404();
			return;
		}

        try{
            
            $image_crud = new image_CRUD();
    
            $image_crud->set_primary_key_field('id');
            $image_crud->set_url_field('url');
            $image_crud->set_title_field('title');
            $image_crud->set_table('photos')
            ->set_relation_field('category_id')
            ->set_ordering_field('position')
            ->set_image_path('assets/img');


            $output = $image_crud->render();

        
            $this->load->view('subview.php',$output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function _assets() {

        if ($this->uri->segment(1) != 'admin') {
            $this->_error_404();
            return;
        }

        try{
            
            $crud = new grocery_CRUD();
                        
            $crud->set_theme('flexigrid');
            $crud->set_subject('Assets');
            $crud->set_table('assets');

            $output = $crud->render();

            $this->load->view('subview', $output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function _layout_assets() {

        if ($this->uri->segment(1) != 'admin') {
            $this->_error_404();
            return;
        }

        try{
            
            $crud = new grocery_CRUD();
                        
            $crud->set_theme('flexigrid');
            $crud->set_subject('Assets');
            $crud->set_table('layout_assets');
            $crud->display_as('asset_id','Asset');
            $crud->set_relation('asset_id','assets','{sub_path}{name}.{type}');

            $output = $crud->render();

            $this->load->view('subview', $output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

    public function _page_assets() {

        if ($this->uri->segment(1) != 'admin') {
            $this->_error_404();
            return;
        }

        try{
            
            $crud = new grocery_CRUD();
                        
            $crud->set_theme('flexigrid');
            $crud->set_subject('Assets');
            $crud->set_table('page_assets');
            $crud->display_as('asset_id','Asset');
            $crud->set_relation('asset_id','assets','{sub_path}{name}.{type}');
            $crud->display_as('page_id','Page');
            $crud->set_relation('page_id','pages','{id}-{name}');

            $output = $crud->render();

            $this->load->view('subview', $output);

        }catch(Exception $e){
            show_error($e->getMessage().' --- '.$e->getTraceAsString());
        }
    }

}