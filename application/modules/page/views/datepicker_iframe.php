<div class="row" id="datepickerIframe" data-baseurl="<?php echo site_url(); ?>">
	<div class="col-lg-offset-2 col-lg-8">
		<h1> iFrame Datepicker Page </h1>

		<form action="<?php echo site_url('datepicker_iframe'); ?>" class="form form-inline" role="form" method="post">
		    <table class="table">
		      <tbody>
		        <!-- DATUM  -->
		        <tr>
		          <td><label for="inputDatum" class="col-xs-4">Datum</label></td>
		          <td>
				    <?php 
				      echo Modules::run('iframe_datepicker', 
				        array(
				          'selector'            => 'Datum', 
				          'input_hidden_value'  => $form_data->datum
				        )
				      ); 
				    ?>
			      </td>
		          <td>
				    <select class="selectpicker"  id="selectpickerDatum"data-live-search="true">
		              <optgroup label="From Now">
		                <?php foreach ($date_modifiers['from_now'] as $key => $modifier) { ?>
		                  <option><?php echo $modifier; ?></option>
		                <?php } ?>
		              </optgroup>
		              <optgroup label="From Selected Date">
		                <?php foreach ($date_modifiers['from_selected_date'] as $key => $modifier) { ?>
		                  <option><?php echo $modifier; ?></option>
		                <?php } ?>
		              </optgroup>
				    </select>
				  </td>
				  <td></td>
				  <td></td>
				  <td></td>
			    </tr>
		      </tbody>
		    </table>

<hr>

		    <!-- SUBMIT -->
		    <div class="row">
		      <div class="col-lg-12">
		      <div style="width: 100%;  text-align: center;">
		        <div style="margin: 0 auto; display: inline-block;">
		          <button type="submit" class="btn btn-success btn-lg" name="submit" id="submit" value="submited" >Submit Form</button>
		        </div>
		      </div>
		      </div>
		    </div>

		</form>
	</div>
</div>