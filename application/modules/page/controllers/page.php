<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     CI 2.2.1 HMVC Bootstrap SASS SBAdmin2 GroceryCRUD Ion-Auth  
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */
// TO DO: extends Frontend_Controller
class Page extends MX_Controller
{

    protected $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('page_m');
        $this->data['pages'] = $this->page_m->get();
        $this->data['pages_slugs'] = $this->page_m->get_pages_slugs();
        // Redirektiraj na odredenata strana pritoa da se izbegni uri segmentot 'page' 
        $this->_page_redirect();
    }

    public function index()
    {/*!Index mora da e prazen oto se vcituva po default, namesto ova se povikuva privatnata _index()*/}


    private function _page_redirect(){

        $slug = ($this->uri->segment(1))? $this->uri->segment(1) : '';
        $this->data['page'] = $this->page_m->get_by_slug($slug);

        if (in_array($slug, $this->data['pages_slugs'])) {
            $method = '_'.$slug;
            if (method_exists($this, $method)) {
                //Call private _method()
                $this->$method();
            }
            else {
                //Ne e napravena _custom() funkcija za ovaa strana
                $this->_error_404();
            }
        } elseif ($slug == '' || $slug == 'index') {
            $this->_index();
        } else {
            $this->_error_404();
        }

    }/*end fun _page_redirect()*/

    public function _index()
    {
        $view = 'index';
        $data = array();
        $data['subview_data'] = new StdClass();
        $data['subview_data']->layout = 'basic';
        $data['subview_data']->pages = $this->page_m->get();

        $this->_render_page($view, $data) ;
    }

    private function _error_404()
    {

        $message = '';
        $message .= '<p>';
        $message .= 'Page not found!';
        $message .= '</p>';
        $message .= '<p>';
        $message .= 'Go back to site ';
        $message .= '<a href="' . site_url() . '">';
        $message .= site_url();
        $message .= '</a>';
        $message .= '</p>';

        $data = array(
            'content' => 'page/error_404',
            'heading' => 'Error 404',
            'message' => $message
        );
        $this->load->view('errors/error_404', $data);
    }

    private function _home(){
        $view = 'home';
        $data = array();
        $data['subview_data'] = new StdClass();
        $data['subview_data']->layout = 'basic';

        $this->_render_page($view, $data) ;
    }    

    private function _about(){
        $view = 'about';
        $data = array();
        $data['subview_data'] = new StdClass();
        $data['subview_data']->layout = 'basic';
        $this->_render_page($view, $data) ;
    }    

    private function _contact(){
        $view = 'contact';
        $data = array();
        $data['subview_data'] = new StdClass();
        $data['subview_data']->layout = 'basic';
        $this->_render_page($view, $data) ;
    }

    private function _login(){
        redirect(site_url('client/login'));
    }
    
    private function _datepicker_iframe(){
        $view = 'datepicker_iframe';
        $data = array();
        $data['subview_data'] = new StdClass();
        $data['subview_data']->layout = 'basic';

        $form_data = new stdClass();
        $form_data->datum           = '';

        if ($this->input->post('submit') == 'submited') {
            
            $post = $this->input->post();

            $form_data->datum            = ( $post['inputHiddenDatum'] )       ? $post['inputHiddenDatum']        : (new DateTime())->format('Y-m-d');

        }/*end if submit*/

        $this->load->model('carbon/carbon_m');
        $date_modifiers = $this->carbon_m->get_date_modifiers();

        $data['subview_data']->form_data    = $form_data;
        $data['subview_data']->date_modifiers = $date_modifiers;

        $this->_render_page($view, $data) ;
    }//end fun _datepicker_iframe()


    function _render_page($view, $data=null) {

        $data['module'] = strtolower(get_class($this));
        $data['subview'] = $view;

        // Get Page assets from model
        $this->load->model('assets_manager/assets_m');
        if (isset($this->data['page']->id)) {
            $data['page_styles'] = $this->assets_m->get_styles($this->data['page']->id);
            $data['page_scripts'] = $this->assets_m->get_scripts($this->data['page']->id);
        } else {
            $data['page_styles'] = array();
            $data['page_scripts'] = array();
        }

        $layout = $data['subview_data']->layout;

        echo Modules::run('layout/_'.$layout, $data);

    }

}