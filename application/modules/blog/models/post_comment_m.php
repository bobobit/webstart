<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     DC HMVC
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Post_Comment_m extends MY_Model
{

    protected $_table_name = 'post_comments';
    protected $_order_by = 'created DESC';

    public function __construct()
    {
        parent::__construct();
    }

    
    public function get_by_post_id($post_id){
        $where = array('post_id' => $post_id);
        return parent::get_by($where, $single = FALSE);
    }

    public function get_recent($limit){
        
        $this->db->limit($limit);

        return parent::get($id = NULL, $single = FALSE);
    }

}