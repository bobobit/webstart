<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     CI 2.2.1 HMVC Ion-Auth GroceryCRUD Bootstrap
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Renamethis_m extends MY_Model
{

    protected $_table_name = 'renamethis';
    protected $_order_by = 'renamethis';

    public function __construct()
    {
        parent::__construct();
    }

}