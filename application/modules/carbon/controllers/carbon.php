<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     CI 2.2.1 HMVC Bootstrap SASS SBAdmin2 GroceryCRUD Ion-Auth
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Carbon extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('dt');
    }

    public function index()
    {
        $data = array(
            'content' => 'renamethis/index'
        );



        $data['dt'] = $this->dt;



        $this->load->view('index', $data);
    }

    /*Od scripts.js SelectPicker Datum*/
    public function ajax_datum_selectpicker(){

        $post = $this->input->post();

        $carbon = new DT($post['input_hidden_value']);//nova instanca od odbraniot datum

        switch ($post['selected']) { //selektiran modifier

            /*From selected date*/
            case 'day forward':{
                $carbon->addDay();
                break;
            }
            case 'day backward':{
                $carbon->subDay();
                break;
            }
            case 'start of week':{
                $carbon->startOfWeek();
                break;
            }
            case 'end of week':{
                $carbon->endOfWeek();
                break;
            }
            case '2nd day of week':
            case '3rd day of week':
            case '4th day of week':
            case '5th day of week':
            case '6th day of week':
            {
                $selected = $post['selected'];
                $count = intval(substr($selected, 0, 1), 10) - 1;
                $carbon->startOfWeek();
                $carbon->addDays($count);
                break;
            }
            case 'week forward':
            case '2 week forward':
            case '3 week forward':
            case '4 week forward':
            case '5 week forward':
            {
                $selected = $post['selected'];
                $count = intval(substr($selected, 0, 1), 10);

                if ($count == 0) {
                    $carbon->next();
                    break;
                }

                for ($i=0; $i < $count; $i++) { 
                    $carbon->next();
                }
                break;
            }
            case 'week backward':
            case '2 week backward':
            case '3 week backward':
            case '4 week backward':
            case '5 week backward':
            {
                $selected = $post['selected'];
                $count = intval(substr($selected, 0, 1), 10);

                if ($count == 0) {
                    $carbon->previous();
                    break;
                }

                for ($i=0; $i < $count; $i++) { 
                    $carbon->previous();
                }
                break;
            }
            case 'start of month':{
                $carbon->startOfMonth();
                break;
            }
            case 'end of month':{
                $carbon->endOfMonth();
                break;
            }
            case 'month forward':{
                $carbon->addMonth();
                break;
            }
            case 'month backward':{
                $carbon->subMonth();
                break;
            }
            case 'first of quarter':{
                $carbon->firstOfQuarter();
                break;
            }
            case 'last of quarter':{
                $carbon->lastOfQuarter();
                break;
            }
            case 'start of year':{
                $carbon->startOfYear();
                break;
            }
            case 'end of year':{
                $carbon->endOfYear();
                break;
            }
            case 'year forward':{
                $carbon->addYear();
                break;
            }
            case 'year backward':{
                $carbon->subYear();
                break;
            }
            case 'start of decade':{
                $carbon->startOfDecade();
                break;
            }
            case 'end of decade':{
                $carbon->endOfDecade();
                break;
            }
            case 'decade forward':{
                $carbon->addYears(10);
                break;
            }
            case 'decade backward':{
                $carbon->subYears(10);
                break;
            }
            case 'start of century':{
                $carbon->startOfCentury();
                break;
            }
            case 'end of century':{
                $carbon->endOfCentury();
                break;
            }



            /*From NOW*/
            case 'today':{
                $carbon = new DT('now');
                break;
            }
            case 'tomorrow':{
                $carbon = new DT('now');
                $carbon->addDay();
                break;
            }
            case 'yesterday':{
                $carbon = new DT('now');
                $carbon->subDay();
                break;
            }
            case 'day after tomorrow':{
                $carbon = new DT('now');
                $carbon->addDay(2);
                break;
            }
            case 'day before yesterday':{
                $carbon = new DT('now');
                $carbon->subDays(2);
                break;
            }
            case 'next week':{
                $carbon = new DT('now');
                $carbon->next();
                break;
            }
            case 'previous week':{
                $carbon = new DT('now');
                $carbon->previous();
                break;
            }
            case 'next month':{
                $carbon = new DT('now');
                $carbon->addMonth();
                break;
            }
            case 'previous month':{
                $carbon = new DT('now');
                $carbon->subMonth();
                break;
            }
            case 'next year':{
                $carbon = new DT('now');
                $carbon->addYear();
                break;
            }
            case 'previous year':{
                $carbon = new DT('now');
                $carbon->subYear();
                break;
            }          
            
            default:
                break;
        }//end switch
        
        $datum = $carbon->toDateString();

// file_put_contents('test.txt', $datum);

        echo json_encode(array('modified_date' => $datum));

    }//end fun ajax_custom_izvod_selectpicker()



}