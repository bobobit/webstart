<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     CI 2.2.1 HMVC Ion-Auth GroceryCRUD Bootstrap
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Carbon_m extends MY_Model
{

    protected $_table_name = 'carbon';
    protected $_order_by = 'position';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_date_modifiers(){

    	return array(
            'from_now' => array(
                '-- modify date --',
                'today',
                'tomorrow',
                'yesterday',
                'day after tomorrow',
                'day before yesterday',
                'next week',
                'previous week',
                'next month',
                'previous month',
                'next year',
                'previous year',
            ),
            'from_selected_date' => array(
                'day forward',
                'day backward',
                'start of week',
                '2nd day of week',
                '3rd day of week',
                '4th day of week',
                '5th day of week',
                '6th day of week',
                'end of week',
                'week forward',
                '2 week forward',
                '3 week forward',
                '4 week forward',
                '5 week forward',
                'week backward',
                '2 week backward',
                '3 week backward',
                '4 week backward',
                '5 week backward',
                'start of month',
                'end of month',
                'month forward',
                'month backward',
                'first of quarter',
                'last of quarter',
                'start of year',
                'end of year',
                'year forward',
                'year backward',
                'start of decade',
                'end of decade',
                'decade forward',
                'decade backward',
                'start of century',
                'end of century',
            )

        );//end return
    }//end fun get_date_modifiers()

/*end class*/}