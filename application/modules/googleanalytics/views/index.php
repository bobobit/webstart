<script language="javascript" type="text/javascript">
  function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }
</script>
<div class="row">
  <div class="col-lg-12">
    <div class="panel-group" id="accordionGoogleAnalytics" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordionGoogleAnalytics" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Google Analytics Embed API
            </a>
          </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">
            <iframe id="iframeGoogleAnalyticsAPI"  name="iframeGoogleAnalyticsAPI"
              width="100%"
              height="430px"
              src="<?php echo site_url('googleanalytics/' . $analytics); ?>"
              frameborder="0"
              scrolling="no" 
              onload='javascript:resizeIframe(this);'
              >
            </iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>