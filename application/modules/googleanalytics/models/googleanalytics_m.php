<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency One Page Bootstrap Template
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Googleanalytics_m extends MY_Model
{

    protected $_table_name = 'renamethis';
    protected $_order_by = 'renamethis';

    public function __construct()
    {
        parent::__construct();
    }

}