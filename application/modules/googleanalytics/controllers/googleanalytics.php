<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency One Page Bootstrap Template
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Googleanalytics extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index($data=array())
    {

        $data['analytics'] = (isset($data['analytics']))? $data['analytics'] : '';

        $this->load->view('index', $data);
    }

    public function tracking(){
        $data = array(
            'content' => 'googleanalytics/tracking'
        );
        $this->load->view('tracking', $data);
    }    

    public function embedapi(){
        $data = array(
            'content' => 'googleanalytics/embedapi'
        );
        $this->load->view('embedapi', $data);
    }

    public function apijsauthentication(){
        $data = array(
            'content' => 'googleanalytics/apijsauthentication'
        );
        $this->load->view('apijsauthentication', $data);
    }

}