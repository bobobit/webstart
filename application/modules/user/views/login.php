<style>
  .login-panel {
      margin-top: 25%;
  }
</style>
<!-- Page Content -->
<div class="container">

<div class="modal show">
  <div class="modal-dialog modal-vertical-centered">
    <div class="modal-content login-panel ">
      <div class="modal-body">

          <div class="panel panel-default">
              <div class="panel-heading">
                  <h3 class="panel-title"><?php echo lang('login_heading');?></h3>
                  <?php if($this->session->flashdata('message')) { ?>
                  <i class="text-danger"><?php echo $this->session->flashdata('message'); ?></i>
                  <?php } ?>
              </div>
              <div class="panel-body">
                  <?php echo form_open("user/login", array('role' => 'form', 'id' => 'login_form'));?>
                      <fieldset>
                          <div class="form-group">
                              <input class="form-control" placeholder="<?php echo lang('login_identity_label');?>" name="<?php echo $identity['name']; ?>" type="<?php echo $identity['type']; ?>" autofocus>
                          </div>
                          <div class="form-group">
                              <input class="form-control" placeholder="<?php echo lang('login_password_label');?>" name="<?php echo $password['name']; ?>" type="<?php echo $password['type']; ?>" value="">
                          </div>
                          <div class="checkbox">
                              <label>
                                  <input name="remember" id="remember" type="checkbox" value="Remember Me"><?php echo lang('login_remember_label');?>
                              </label>
                          </div>
                          <div class="form-group">
                                <?php echo form_submit(array(
                                        'type'        => 'submit',
                                        'name'        => 'submit',
                                        'class'       => 'btn btn-lg btn-success btn-block',
                                        'id'          => 'submit',
                                        'value'       => lang('login_submit_btn'),
                                      ));
                                ?>
                          </div>
                      </fieldset>
                  <?php echo form_close();?>

                  <a   href="forgot_password?forgot_password=1"><?php echo lang('login_forgot_password');?></a>
                  <a   class="pull-right" href="<?php echo site_url(); ?>"><?php echo lang('login_cancel');?></a>
              </div>
          </div>
          
          <p><a class="" href="<?php echo site_url('client/signup'); ?>"><?php echo lang('signup_user_link');?></a> </p>

      </div>
  </div>      
  </div>
</div>
</div>
<!-- /.container -->