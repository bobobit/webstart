<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<!-- Ovaa meta e za IE da raboti so JSON.stringify() -->
		<meta http-equiv="X-UA-Compatible" content="IE=8" />
		<title>Iframe Datepicker</title>


		<link href="<?php echo site_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
		<link href="<?php echo site_url('assets/css/style.css'); ?>" rel="stylesheet">
		<link href="<?php echo site_url('assets/widgets/calendar/jquery-ui.css'); ?>" rel="stylesheet">


		<style>

			body {
				background-color: rgba(127,127,127, 0.001);
			}

			input.datepicker {
				border-radius: 3px;
				display: block;
				width: 100%;
			}

			div.ui-datepicker {
				font-size:13.2px;
			}


		</style>

	</head>
	<body data-site-url="<?php echo site_url(); ?>">

		<input type="text" class="datepicker form-control" id="datepicker" placeholder="Y-m-d">

		<!-- FOOTER  -->
		<script src="<?php echo site_url('assets/widgets/calendar/external/jquery/jquery.js'); ?>"></script>
		<script src="<?php echo site_url('assets/widgets/calendar/jquery-ui.js'); ?>"></script>
		<script src="<?php echo site_url('assets/widgets/calendar/dt.js'); ?>"></script>
		<script src="<?php echo site_url('assets/js/bootstrap.min.js'); ?>"></script>


<script>

	var selector = "<?php echo $selector; ?>";

	$(function(){/*Document ready*/

		/*DATEPICKER DATE*/
		var iframe = $("iframe#iframe" + selector, window.parent.document);
		var input_hidden = $('#inputHidden' + selector, window.parent.document);

		var ifr_height = iframe.height();
		var ifr_width = iframe.width();

		var dp = $( "#datepicker");
		dp.val(input_hidden.val());

		dp.click(function(){
			iframe.height('270px').width('250px');
		});

		dp.datepicker( { 

			dateFormat: "yy-mm-dd",
			defaultDate: input_hidden.val(),
			
			onSelect: function(event, ui){

				var year = parseInt(ui.selectedYear,10);
				var month = parseInt(ui.selectedMonth,10) + 1;/*mesecite pocnuvaat od 0 do 11*/
				var day = parseInt(ui.selectedDay,10);

				var sMonth = (month < 10)? '0'+month : month;
				var sDay = (day < 10)? '0'+day : day;
				input_hidden.val(year + '-' + sMonth + '-' + sDay);
				iframe.height(ifr_height).width(ifr_width);

			}
			
		} );/*end datepicker*/
		

	});/*end document ready*/

</script>

	</body>
</html>