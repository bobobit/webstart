<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     CI 2.2.1 HMVC Bootstrap SASS SBAdmin2 GroceryCRUD Ion-Auth
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Iframe_datepicker extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    public function index() {
        $this->_iframe_tag(func_get_arg(0));
    }

    private function _iframe_tag() {
        $this->load->view('iframe_tag', func_get_arg(0));
    }

    public function iframe_contents($selector) {
        $data['selector'] = $selector;
        $this->load->view('iframe_contents', $data);
    }

}