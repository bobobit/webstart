<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     CI 2.2.1 HMVC Bootstrap SASS SBAdmin2 GroceryCRUD Ion-Auth
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Admin extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('page/page_m');
    }

    public function index() {

        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'admin';
        // $data['subview_data']->pages = $this->data['pages'];

        $this->_render_page('index', $data);

    }

    public function dashboard() {

        $this->index();

    }

    public function pages() {
        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'admin';
        $data['table'] = 'pages';

        $this->_render_page('table_subview', $data);
    }   

    public function iframe_pages() {
        echo Modules::run('grocerycrud/_admin_pages'); 
    }


    public function posts() {
        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'admin';
        $data['table'] = 'posts';

        $this->_render_page('table_subview', $data);
    }   

    public function iframe_posts() {
        echo Modules::run('grocerycrud/_admin_posts');
    }

    public function post_comments() {
        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'admin';
        $data['table'] = 'post_comments';

        $this->_render_page('table_subview', $data);
    }   

    public function iframe_post_comments() {
        echo Modules::run('grocerycrud/_admin_post_comments');
    }

    public function categories() {
        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'admin';
        $data['table'] = 'categories';

        $this->_render_page('table_subview', $data);
    }   

    public function iframe_categories() {
        echo Modules::run('grocerycrud/_admin_categories');
    }


    public function iframe_photos() {
        echo Modules::run('grocerycrud/_admin_photos'); 
    }

    public function upload_photo() {

        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'admin';
        $data['subview_data']->pages = $this->data['pages'];
        $data['subview_data']->page = $this->page_m->get_by_id($this->uri->segment(3));

        $this->_render_page('upload_photo', $data);
    }   

    public function iframe_upload_photo() {
        echo Modules::run('grocerycrud/_admin_upload_photo'); 
    }


    public function assets() {
        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'admin';
        $data['table'] = 'assets';

        $this->_render_page('table_subview', $data);
    }   

    public function iframe_assets() {
        echo Modules::run('grocerycrud/_assets');
    }

    public function layout_assets() {
        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'admin';
        $data['table'] = 'layout_assets';

        $this->_render_page('table_subview', $data);
    }   

    public function iframe_layout_assets() {
        echo Modules::run('grocerycrud/_layout_assets');
    }

    public function page_assets() {
        $data['subview_data'] = new stdClass();
        $data['subview_data']->layout = 'admin';
        $data['table'] = 'page_assets';

        $this->_render_page('table_subview', $data);
    }   

    public function iframe_page_assets() {
        echo Modules::run('grocerycrud/_page_assets');
    }



    public function profiler_logs(){

        $data['contents'] = file_get_contents(APPPATH . 'logs/profiler.php');

        $this->load->view('admin/profiler_logs', $data);
    }


    /*RENDER ADMIN PAGE*/
    function _render_page($view, $data=null) {

        $data['module'] = strtolower(get_class($this));
        $data['subview'] = $view;

        $layout = $data['subview_data']->layout;

        echo Modules::run('admin_layout/_'.$layout, $data);

    }

}