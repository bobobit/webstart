<!-- Page Content -->
<style type="text/css">
  .container-fluid{
    margin: 0;
    padding: 0;
  }  
  #page-wrapper{
    margin: 0;
    padding-right: 33px;
  }

  iframe {
    min-height: 200px;
  }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-offset-5 col-lg-7">
                <h3>Upload photos to <?php echo $page->title; ?></h3>
            </div>
            <div class="col-lg-offset-2 col-lg-10">

            	<iframe id="iframeUpload_photo"  name="iframeUpload_photo"
            	width="100%"
            	src="<?php echo site_url('admin/iframe_upload_photo/' . $this->uri->segment(3)); ?>"
              frameborder="0"
              scrolling="yes" 
              onload='javascript:resizeIframe(this);'
              >
            	</iframe>
              <!-- <a class="btn btn-primary" href="<?php echo site_url('admin/photos'); ?>"><i class="fa fa-edit fa-fw"></i> Go back to edit list</a> -->
            </div>
            <div class="col-lg-12">
              <hr>
            </div>
            <div class="col-lg-offset-2 col-lg-10">
                <h3>Edit Photos</h3>
            </div>
            <div class="col-lg-offset-2 col-lg-10">
              <iframe id="iframePhotos"  name="iframePhotos"
              width="100%"
              src="<?php echo site_url('admin/iframe_photos/' . $this->uri->segment(3)); ?>"
              frameborder="0"
              scrolling="yes" 
              onload='javascript:resizeIframe(this);'
              >
              </iframe>
            </div>

        </div>
    </div>
</div>
<script language="javascript" type="text/javascript">
  function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }
</script>