<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     CI 2.2.1 HMVC Bootstrap SASS SBAdmin2 GroceryCRUD Ion-Auth
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Assets_manager extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('assets');
        /*SEND BASE PATH TO Assets Library - ako se koristi objektno*/
        $this->assets->set_base_url(base_url());

    }

    public function index()
    {
        $data = array(
            'content' => 'assets/index'
        );
        $this->load->view('index', $data);
    }    

    public function js($scripts) {

        if (empty($scripts)) {
           $this->load->view('index');
           return;
        }

        $data['js'] = Assets::scripts(base_url(), $scripts);

        $this->load->view('js', $data);
    }

    public function css($styles) {
        
        if (empty($styles)) {
           $this->load->view('index');
           return;
        }

        $data['css'] = Assets::styles(base_url(), $styles);

        $this->load->view('css', $data);
    }


}