<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     CI 2.2.1 HMVC Ion-Auth GroceryCRUD Bootstrap
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Assets_m extends MY_Model
{

    protected $_table_name = 'assets';
    protected $_order_by = 'id';

    public function __construct()
    {
        parent::__construct();
    }

    private function get_assets($page_id, $asset_type){

        $this->db->select('*');
        $this->db->from('assets');
        $this->db->join('page_assets', 'assets.id=page_assets.asset_id');
        $this->db->where('page_assets.page_id', $page_id); 
        $this->db->where('assets.type', $asset_type);
        $this->db->order_by('position');
        $assets = $this->db->get()->result();

        return $assets;
    }

    public function get_scripts($page_id){

        return $this->get_assets($page_id, 'js');

    }

    public function get_styles($page_id){

        return $this->get_assets($page_id, 'css');

    }


    private function get_layout_assets($layout_name, $asset_type){

        $this->db->select('*');
        $this->db->from('assets');
        $this->db->join('layout_assets', 'assets.id=layout_assets.asset_id');
        $this->db->where('layout_assets.layout_name', $layout_name); 
        $this->db->where('assets.type', $asset_type);
        $this->db->order_by('position');
        $assets = $this->db->get()->result();

        return $assets;
    }

    public function get_layout_scripts($layout_name){

        return $this->get_layout_assets($layout_name, 'js');

    }

    public function get_layout_styles($layout_name){

        return $this->get_layout_assets($layout_name, 'css');

    }


}