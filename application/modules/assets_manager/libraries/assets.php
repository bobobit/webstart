<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     CI 2.2.1 HMVC Bootstrap SASS SBAdmin2 GroceryCRUD Ion-Auth
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Assets {

    protected $js;
    protected $css;
    protected $base_url;
    protected $assets_dirname;
    protected $cdn;
    const ASSETS_DIR = 'assets';


    public function __construct() {
        $this->assets_dirname = 'assets';
        $this->js = array();
        $this->css= array();
    }

    private function prepend_path($assets){

        foreach ($assets as $asset) {

            $url = '';
            $url .= $this->get_base_url();
            $url .= $this->get_assets_dirname();
            $url .= '/';
            $url .= $asset->sub_path;
            $url .= $asset->name;
            $url .= '.';
            $url .= $asset->type;

            $asset->url = $url;
        }

        return $assets;
    }


    public function get_js(){
        return $this->js;
    }//end fun get_js()
    public function set_js( $js ){
        $this->js = $this->prepend_path($js);
    }//end fun set_js()
    public function get_css(){
        return $this->css;
    }//end fun get_css()
    public function set_css( $css ){
        $this->css = $this->prepend_path($css);
    }//end fun set_css()
    private function get_assets_dirname(){
        return $this->assets_dirname;
    }//end fun get_assets_dirname()
    public function set_assets_dirname( $assets_dirname ){
        $this->assets_dirname = $assets_dirname;
    }//end fun set_path()
    private function get_base_url(){
        return $this->base_url;
    }//end fun get_base_url()
    public function set_base_url( $base_url ){
        $this->base_url = $base_url;
    }//end fun set_base_url()
    public function get_cdn(){
        return $this->cdn;
    }//end fun get_cdn()
    public function set_cdn( $cdn ){
        $this->cdn = $cdn;
    }//end fun set_cdn()

    private static function prepend_path_static($base_url, $assets){

        foreach ($assets as $asset) {

            $url = '';
            $url .= $base_url;
            $url .= self::ASSETS_DIR;
            $url .= '/';
            $url .= $asset->sub_path;
            $url .= $asset->name;
            $url .= '.';
            $url .= $asset->type;

            $asset->url = $url;
        }

        return $assets;
    }

    public static function styles($base_url, $styles){

        return self::prepend_path_static($base_url, $styles);

    }

    public static function scripts($base_url, $scripts){

        return self::prepend_path_static($base_url, $scripts);

    }


/*ASSETS*/}