<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Start Web</title>

    <!-- Assets Styles -->
    <?php echo Modules::run('assets_manager/css', $styles); ?>

    <!-- Google Analytics Tracking Code -->
    <?php echo Modules::run('googleanalytics/tracking'); ?>

</head>

<body>