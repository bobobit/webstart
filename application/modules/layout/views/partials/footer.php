
	<!-- FOOTER -->
	<footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; <?php //echo lang('site_title'); ?> <?php echo date('Y'); ?></span>
                </div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <ul class="list-inline">
                        <li><a href="#">Privacy Policy</a>
                        </li>
                        <li><a href="#">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- Assets Scripts -->
    <?php echo Modules::run('assets_manager/js', $scripts); ?>

</body>

</html>