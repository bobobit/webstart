<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Start Web</title>

        <!-- Assets Styles -->
    <?php echo Modules::run('assets_manager/css', $styles); ?>

    <!-- CDN Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> -->
    <!-- Bootstrap Core CSS -->
    <!-- <link href="<?php echo site_url('assets/css/bootstrap.css'); ?>" rel="stylesheet"> -->
    <!-- Custom CSS -->
    <!-- <link href="<?php echo site_url('assets/css/style.css'); ?>" rel="stylesheet"> -->
    <!-- Font-awesome Fonts -->
    <!-- <link href="<?php echo site_url('assets/fonts/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css"> -->

    <!-- Google Analytics Tracking Code -->
    <?php echo Modules::run('googleanalytics/tracking'); ?>

</head>

<body>