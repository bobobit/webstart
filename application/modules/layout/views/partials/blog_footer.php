
	<!-- FOOTER -->
	<footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; <?php //echo lang('site_title'); ?> <?php echo date('Y'); ?></span>
                </div>
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <ul class="list-inline">
                        <li><a href="#">Privacy Policy</a>
                        </li>
                        <li><a href="#">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- Assets Scripts -->
    <?php echo Modules::run('assets_manager/js', $scripts); ?>

    <!-- jQuery CDN -->
    <!-- <script src="//code.jquery.com/jquery-1.11.2.min.js"></script> -->
    <!-- jQuery -->
    <!-- <script>window.jQuery || document.write('<script src="<?php echo site_url('assets/js/jquery.js'); ?>"><\/script>')</script> -->


    <!-- Bootstrap JS CDN -->
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script> -->
    <!-- Bootstrap Core JavaScript -->
    <!-- <script>if(typeof($.fn.modal) === 'undefined') {document.write('<script src="<?php echo site_url('assets/js/bootstrap.min.js'); ?>"><\/script>')}</script> -->
  
    
    <!-- Custom Scripts -->
    <!-- <script src="<?php echo site_url('assets/js/scripts.js'); ?>"></script> -->
    

</body>

</html>