<?php $this->load->view('partials/header', $output); ?>
<?php $this->load->view('partials/navigation', $output); ?>

<?php $this->load->view($module . '/' . $subview, $subview_data); ?>

<?php $this->load->view('partials/footer', $output); ?>
