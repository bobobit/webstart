<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     CI 2.2.1 HMVC Bootstrap SASS SBAdmin2 GroceryCRUD Ion-Auth
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Layout extends Frontend_Controller
{

    public function __construct() {
        parent::__construct();
        $this->load->model("page/page_m");
        $this->data['menu'] = $this->page_m->get_public_menu();
    }

    public function index() {
        redirect('/');
    }

    public function _basic($data) {

        $data = $this->output_data($data);

        $this->load->view('basic', $data);
    }

    public function _blog($data) {

        $data = $this->output_data($data);

        $this->load->view('blog', $data);
    }

    private function output_data($data){

        // Get Layout assets from model
        $this->load->model('assets_manager/assets_m');
        $layout_styles = $this->assets_m->get_layout_styles($data['subview_data']->layout);
        $layout_scripts = $this->assets_m->get_layout_scripts($data['subview_data']->layout);

        // Site asseti sobrani vo edno, i od layoutot i od page ako ima nesto posebno
        $styles  = array_merge($layout_styles,  $data['page_styles']);
        $scripts = array_merge($layout_scripts, $data['page_scripts']);

        // Sort merged arrays by asset->position
        usort($styles, function($a, $b)
        {
            return $a->position > $b->position;
        });
        usort($scripts, function($a, $b)
        {
            return $a->position > $b->position;
        });

        $data['output'] = array(
            'menu'      => $this->data['menu'],
            'styles'    => $styles,
            'scripts'   => $scripts
        );

        return $data;
    }
}