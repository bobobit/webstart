<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     CI 2.2.1 HMVC Bootstrap SASS SBAdmin2 GroceryCRUD Ion-Auth
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Admin_layout extends Admin_Controller
{

    // TO DO: ova ce go premestam vo MY_Controller ko ce go napravam
    public $data = array();

    public function __construct() {
        parent::__construct();
        $this->load->model("page/page_m");
        $this->data['menu'] = $this->page_m->get_admin_menu();
    }

    public function index() {
        redirect('/');
    }

    public function _admin($data) {
        $this->load->view('admin', $data);
    }


    public function _authentication($data) {
        $this->load->view('authentication', $data);
    }
}