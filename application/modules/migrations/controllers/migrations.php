<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     CI 2.2.1 HMVC Bootstrap SASS SBAdmin2 GroceryCRUD Ion-Auth
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Migrations extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('migration');
    }

    public function index()
    {
        $data = array(
            'content' => 'renamethis/index'
        );

        $message = '';

        if (! $this->migration->current()) {
            show_error($this->migration->error_string());
        }
        else {
            $message = "Migracijata e uspesna";
        }

        $data['message'] = $message;

        $this->load->view('index', $data);
    }

}