<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_cdn_to_assets extends CI_Migration {

	public function up()
	{
		$fields = (array(
			'cdn' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
				'default' =>  NULL
			)
		));
		$this->dbforge->add_column('assets', $fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('assets', 'cdn');
	}
}