<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_permission_level_to_groups extends CI_Migration {

	public function up()
	{
		$fields = (array(
			'permission_level' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => 0
			)
		));
		$this->dbforge->add_column('groups', $fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('groups', 'permission_level');
	}
}