<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_post_category extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'post_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'category_id' => array(
				'type' => 'INT',
				'constraint' => 11
			)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('post_category');
	}

	public function down()
	{
		$this->dbforge->drop_table('post_category');
	}
}