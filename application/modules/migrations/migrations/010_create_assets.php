<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_assets extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'type' => array(
				'type' => 'VARCHAR',
				'constraint' => '255'
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'sub_path' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			)

		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('assets');
	}

	public function down()
	{
		$this->dbforge->drop_table('assets');
	}
}