<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_temp_users extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'temp_user_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'user_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '255'
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '255'
			),
			'password' => array(
				'type' => 'VARCHAR',
				'constraint' => '255'
			),
			'additional_data' => array(
				'type' => 'VARCHAR',
				'constraint' => '255'
			),
			'email_key' => array(
				'type' => 'VARCHAR',
				'constraint' => '255'
			),
			'date_email_created' => array(
				'type' => 'DATETIME'
			)
		));
		$this->dbforge->add_key('temp_user_id', TRUE);
		$this->dbforge->create_table('temp_users');
	}

	public function down()
	{
		$this->dbforge->drop_table('temp_users');
	}
}