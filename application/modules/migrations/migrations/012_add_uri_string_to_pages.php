<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_uri_string_to_pages extends CI_Migration {

	public function up()
	{
		$fields = (array(
			'uri_string' => array(
				'type' => 'VARCHAR',
				'constraint' => '255'
			)
		));
		$this->dbforge->add_column('pages', $fields);
		$this->db->query("ALTER TABLE `pages` MODIFY COLUMN `uri_string` VARCHAR(255) AFTER `slug` ");
	}

	public function down()
	{
		$this->dbforge->drop_column('pages', 'uri_string');
	}
}