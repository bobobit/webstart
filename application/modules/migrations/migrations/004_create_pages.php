<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_pages extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'parent_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'slug' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'title' => array(
				'type' => 'VARCHAR',
				'constraint' => '255'
			),
			'template' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'layout' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'position' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'visible' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				'DEFAULT' => 1
			),
			'contents' => array(
				'type' => 'TEXT'
			)
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('pages');
	}

	public function down()
	{
		$this->dbforge->drop_table('pages');
	}
}