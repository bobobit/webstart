<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_page_assets extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'page_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'asset_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'position' => array(
				'type' => 'INT',
				'constraint' => 11
			)

		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('page_assets');
	}

	public function down()
	{
		$this->dbforge->drop_table('page_assets');
	}
}