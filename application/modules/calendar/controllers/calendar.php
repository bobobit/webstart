<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency One Page Bootstrap Template
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Calendar extends Frontend_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('dt');//My DateTime Library
        $this->data['dt'] = $this->dt;
    }

    public function index()
    {
        $data = array(
            'content' => 'calendar/index'
        );

        $this->load->model('page/page_m');
        $this->data['page'] = $this->page_m->get_by_uri_string(uri_string());

        // Get assets from model
        $this->load->model('assets_manager/assets_m');
        $data['styles'] = $this->assets_m->get_styles($this->data['page']->id);
        $data['scripts'] = $this->assets_m->get_scripts($this->data['page']->id);
        
        $data['dt'] = $this->data['dt'];
        $this->load->view('index', $data);
    }

    public function add_sub(){

        $this->dt->setDate(2015, 1, 31);
        $this->dt->set_format('l, F j, Y');

        eh1($this->dt);

        $interval = new DateInterval('P1M');
        $this->dt->add($interval);

        eh1($this->dt); 

        $this->dt->sub($interval);
        
        eh1($this->dt); 
    }

    public function form(){

        if ($this->input->post()) {

            /*POST*/
            $year   = intval($this->input->post('year'),   10);
            $month  = intval($this->input->post('month'),  10);
            $day    = intval($this->input->post('day'),    10);
            $hour   = intval($this->input->post('hour'),   10);
            $minute = intval($this->input->post('minute'), 10);
            $second = intval($this->input->post('second'), 10);
            $date   = $this->input->post('date');
            $time   = $this->input->post('time'); 
            $date_time   = $this->input->post('date_time');

            /*Selektirano vreme*/
            $this->dt->createFromFormat('Y-m-d H:i:s',  $date_time);
            $this->dt->setDate($year, $month, $day);
            $this->dt->setTime($hour, $minute, $second);
            $this->dt->reset();

            /*posle 7 dena*/
            $interval = new DateInterval('P7D');
            $this->dt->add($interval);
            $this->dt->reset();//reset object

        } else {
            
            /*Ako nema POST togas current date-time (NOW)*/

        }/*end if/else post*/

        $data = array(

            'year'      => $this->dt->get_year(),
            'month'     => $this->dt->get_month(),
            'day'       => $this->dt->get_day(),
            'hour'      => $this->dt->get_hour(),
            'minute'    => $this->dt->get_minute(),
            'second'    => $this->dt->get_second(),
            'date'      => $this->dt->get_date(),
            'time'      => $this->dt->get_time(),
            'date_time' => $this->dt->get_date_time()

        );

        $this->load->model('page/page_m');
        $this->data['page'] = $this->page_m->get_by_uri_string(uri_string());

        // Get assets from model
        $this->load->model('assets_manager/assets_m');
        $data['styles'] = $this->assets_m->get_styles($this->data['page']->id);
        $data['scripts'] = $this->assets_m->get_scripts($this->data['page']->id);

        $this->load->view('form', $data);

    }/*end fun form()*/

    public function form_ajax(){

        // ja precekuvam json datata
        $json_data = $this->input->post('json_data');
        // dekodiranje na json vo niza
        $decode = json_decode($json_data, true);
        // na dekodiraniot date-time objekt(niza) mu pristapuvam so klucot dt
        $dt = $decode['dt'];

        // gi ekstraktuvam elementite od dt objektot vo posebni promenlivi
        $year   = intval($dt['year'],   10);
        $month  = intval($dt['month'],  10);
        $day    = intval($dt['day'],    10);
        $hour   = intval($dt['hour'],   10);
        $minute = intval($dt['minute'], 10);
        $second = intval($dt['second'], 10);

        /*Selektirano vreme*/
        $this->dt->createFromFormat('Y-m-d H:i:s', $dt['date'] . ' ' . $dt['time']);
        $this->dt->setDate($year, $month, $day);
        $this->dt->setTime($hour, $minute, $second);
        $this->dt->reset();//resetiranje na objektot

        /*vreme pred 3 meseci 2 nedeli 1 den itn...*/
        $this->dt->modify( '-3 month -2 week -1 day +3 hour +2 minute +10 second' );
        $this->dt->reset();

        // priprema na noviot objekt za pretvorba povtorno vo json
        $array = array(
            'year'   => $this->dt->get_year(),
            'month'  => $this->dt->get_month(),
            'day'    => $this->dt->get_day(),
            'hour'   => $this->dt->get_hour(),
            'minute' => $this->dt->get_minute(),
            'second' => $this->dt->get_second(),
            'pdate'   => $this->dt->get_date(),
            'time'   => $this->dt->get_time(),
            'date_time'  => $this->dt->get_date_time()
        );

        // gi dodavam elementite date i timezone od bazicnata klasa DateTime
        $new_json = array_merge($array, object_to_array($this->dt));

        /*Ovde se pecati json datata sto ja ocekuva ajax skriptata vo form.php*/
        echo json_encode($new_json);

    }/*form_ajax*/

/*END CLASS CALENDAR*/}