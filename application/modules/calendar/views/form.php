<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<!-- Ovaa meta e za IE da raboti so JSON.stringify() -->
		<meta http-equiv="X-UA-Compatible" content="IE=8" />
		<title>Calendar - DateTime</title>


		<?php echo Modules::run('assets_manager/css', $styles); ?>

		<style>

			body {
				background-color: rgba(127,127,127, 0.001);
			}

			input#datepicker {
				border-radius: 3px;
				display: block;
				width: 100%;
			}

			div.ui-datepicker {
				font-size:13.2px;
			}

			/* SPINNER */
			


		</style>

	</head>
	<body data-site-url="<?php echo site_url(); ?>">

		<div class="container">
		
		<form action="<?php echo site_url('calendar/form'); ?>" method="POST">
	
			<!--  DATEPICKER -->
			<div class="date">
				<div class="datepicker"></div>
				<input type="text" id="year" name="year" value="<?php echo $year; ?>">
				<input type="text" id="month" name="month" value="<?php echo $month; ?>">
				<input type="text" id="day" name="day" value="<?php echo $day; ?>">
				<input type="text" id="date" name="date" value="<?php echo $date; ?>">
			</div>

			<!--  SPINNERS -->
			<div class="time">
				<input id="hour" name="hour" value="<?php echo $hour; ?>">
				<input id="minute" name="minute" value="<?php echo $minute; ?>">
				<input id="second" name="second" value="<?php echo $second; ?>">
				<input id="time" name="time" value="<?php echo $time; ?>">
			</div>

			<input type="hidden" id="date_time" name="date_time" value="<?php echo $date_time; ?>">
			<input type="submit" name="submit" value="P7D">

		</form>

		</div><!-- end container  -->
<br>
<br>
		<div>
			<button class="btn btn-success" id="modify_dt">-3 month -2 week -1 day +3 hour +2 minute +10 second</button>
		</div>


		<!-- FOOTER  -->
		<?php echo Modules::run('assets_manager/js', $scripts); ?>


	</body>
</html>