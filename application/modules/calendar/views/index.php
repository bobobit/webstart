<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Datepicker - Only Month Year</title>

		<!-- Assets Manager  -->
		<?php echo Modules::run('assets_manager/css', $styles); ?>
		<?php echo Modules::run('assets_manager/js', $scripts); ?>

		<style>

			body {
				background-color: rgba(127,127,127, 0.001);
			}

			input#datepicker {
				border-radius: 3px;
				display: block;
				width: 100%;
			}

			div.ui-datepicker {
				font-size:13.2px;
			}

			/* SPINNER */
			


		</style>

	</head>
	<body>

		<div class="container">
			
			<!--  DATEPICKER -->
			<div class="date">
				<div class="datepicker"></div>
				<input type="text" id="year">
				<input type="text" id="month">
				<input type="text" id="day">
				<input type="text" id="date">
			</div>

			<!--  SPINNERS -->
			<div class="time">
				<input id="hour">
				<input id="minute">
				<input id="second">
				<input id="time">
			</div>

		</div><!-- end container  -->
		







		<script>

			$(function() {

				/*DATEPICKER DATE*/
				var dp = $( ".datepicker" );
				dp.datepicker( { 
					
					onSelect: function(event, ui){


						var year = parseInt(ui.selectedYear,10);
						var month = parseInt(ui.selectedMonth,10) + 1;/*mesecite pocnuvaat od 0 do 11*/
						var day = parseInt(ui.selectedDay,10);
						$( "#year" ).val(year);
						$( "#month" ).val(month);
						$( "#day" ).val(day);

						var sMonth = (month < 10)? '0'+month : month;
						var sDay = (day < 10)? '0'+day : day;
						$( "#date" ).val(year + '-' + sMonth + '-' + sDay);


						// var date = dp.datepicker( "getDate" );

					}
					
				} );/*end datepicker*/


				/* SPINNER TIME*/
				var sHour	= '00';
				var sMinute = '00';
				var sSecond = '00';

				$( "#time" ).val(sHour + ':' + sMinute + ':' + sSecond);//on doc ready


				var hsp = $( "#hour" );
				var msp = $( "#minute" );
				var ssp = $( "#second" );
				hsp.spinner({
					min: 0,
					max: 23,
					spin: function( event, ui ) {
						var hour = parseInt(ui.value,10);
						sHour = (hour < 10)? '0'+hour : hour;
						$( "#time" ).val(sHour + ':' + sMinute + ':' + sSecond);
					}
				});
				msp.spinner({
					min: 0,
					max: 59,
					spin: function( event, ui ) {
						var minute = parseInt(ui.value,10);
						sMinute = (minute < 10)? '0'+minute : minute;
						$( "#time" ).val(sHour + ':' + sMinute + ':' + sSecond);
					}
				});
				ssp.spinner({
					min: 0,
					max: 59,
					spin: function( event, ui ) {
						var second = parseInt(ui.value,10);
						sSecond = (second < 10)? '0'+second : second;
						$( "#time" ).val(sHour + ':' + sMinute + ':' + sSecond);
					}
				});
 
				


			});/*end doc ready*/
		</script>



	</body>
</html>