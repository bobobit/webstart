<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     Agency One Page Bootstrap Template
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class DT extends DateTime {


	/**
	//DateTime Constants
	const string ATOM = "Y-m-d\TH:i:sP" ;
	const string COOKIE = "l, d-M-Y H:i:s T" ;
	const string ISO8601 = "Y-m-d\TH:i:sO" ;
	const string RFC822 = "D, d M y H:i:s O" ;
	const string RFC850 = "l, d-M-y H:i:s T" ;
	const string RFC1036 = "D, d M y H:i:s O" ;
	const string RFC1123 = "D, d M Y H:i:s O" ;
	const string RFC2822 = "D, d M Y H:i:s O" ;
	const string RFC3339 = "Y-m-d\TH:i:sP" ;
	const string RSS = "D, d M Y H:i:s O" ;
	const string W3C = "Y-m-d\TH:i:sP" ;


	//DateTime Methods
	public __construct ([ string $time = "now" [, DateTimeZone $timezone = NULL ]] )
	public DateTime add ( DateInterval $interval )
	public static DateTime createFromFormat ( string $format , string $time [, DateTimeZone $timezone ] )
	public static array getLastErrors ( void )
	public DateTime modify ( string $modify )
	public static DateTime __set_state ( array $array )
	public DateTime setDate ( int $year , int $month , int $day )
	public DateTime setISODate ( int $year , int $week [, int $day = 1 ] )
	public DateTime setTime ( int $hour , int $minute [, int $second = 0 ] )
	public DateTime setTimestamp ( int $unixtimestamp )
	public DateTime setTimezone ( DateTimeZone $timezone )
	public DateTime sub ( DateInterval $interval )
	public DateInterval diff ( DateTimeInterface $datetime2 [, bool $absolute = false ] )
	public string format ( string $format )
	public int getOffset ( void )
	public int getTimestamp ( void )
	public DateTimeZone getTimezone ( void )
	public __wakeup ( void )
	*/

	const DEFAULT_FORMAT = 'd.m.Y H:i:s';
	const MYSQL_FORMAT = 'Y-m-d H:i:s';//YYYY-MM-DD HH:MM:SS
	const U_FORMAT = 'U';//dava zbir na sekundi
	protected $format;

	private $year;
    private $month;
    private $day;
    private $hour;
    private $minute;
    private $second;
    private $pdate;//private date
    private $time;
    private $date_time;

	public function __construct(){
		parent::__construct();

		$this->init();

		$this->format = self::DEFAULT_FORMAT;

	}
	/*INIT NOW*/
	private function init(){

		$this->set_format('Y');
        $this->year = intval($this . '', 10);
        $this->set_format('m');
        $this->month = intval($this . '', 10);
        $this->set_format('d');
        $this->day = intval($this . '', 10);

        $this->set_format('H');
        $this->hour = intval($this . '', 10);
        $this->set_format('i');
        $this->minute = intval($this . '', 10);
        $this->set_format('s');
        $this->second = intval($this . '', 10);

		$this->set_format('Y-m-d');
        $this->pdate = $this . '';

        $this->set_format('H:i:s');
        $this->time = $this . '';

        $this->set_format('Y-m-d H:i:s');
        $this->date_time = $this . '';
	}
	/*RESET OBJECT*/
	public function reset(){
		$this->init();
	}
	/*TO STRING*/
	public function __toString(){
		return $this->format($this->format);
	}
	/*DATE FORMAT*/
	public function set_format($format){
		$this->format = $format;
	}
	public function get_format(){
		return $this->format;
	}
	/*SETTERS GETTERS*/
	public function set_year($year){
		$this->year = $year;
	}
	public function get_year(){
		return $this->year;
	}
	public function set_month($month){
		$this->month = $month;
	}
	public function get_month(){
		return $this->month;
	}
	public function set_day($day){
		$this->day = $day;
	}
	public function get_day(){
		return $this->day;
	}
	public function set_hour($hour){
		$this->hour = $hour;
	}
	public function get_hour(){
		return $this->hour;
	}
	public function set_minute($minute){
		$this->minute = $minute;
	}
	public function get_minute(){
		return $this->minute;
	}
	public function set_second($second){
		$this->second = $second;
	}
	public function get_second(){
		return $this->second;
	}


	public function set_date($date){
		$this->pdate = $date;
	}
	public function get_date(){
		return $this->pdate;
	}
	public function set_time($time){
		$this->time = $time;
	}
	public function get_time(){
		return $this->time;
	}
	public function set_date_time($date_time){
		$this->date_time = $date_time;
	}
	public function get_date_time(){
		return $this->date_time;
	}




/*END CLASS DT*/}